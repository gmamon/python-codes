#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr  5 09:43:30 2023

Make corner plots for MAMPOSSt runs
@author: gam
"""

import numpy as np
import pandas as pd
import matplotlib as mpl
from matplotlib import pyplot as plt
import os
import sys
import glob
import mathutils as mu
import subprocess
home_dir = os.getenv("HOME") + '/'
# ini_dir = home_dir + 'PAPERS/MAMPOSST/WINGS/'
from ReadChains import ReadCosmoMCchains
from corner_ev import corner

def ReadIni(prefix,ini_dir,labels=True,verbose=0):
    """Read MAMPOSSt initialization file:
        
    arguments:
        prefix: file prefix
        ini_dir: directory of .ini file
        labels: run Labels if True
        
    returns:
        darknormflag,
        darkscaleflag, 
        anisflag,
        darkmodel,
        tracermodel,
        anismodel,
        rfid_dark,
        dictionary of bounds of free parameter bounds,
        labels (list, if labels=True),
        data_file
        
    author: Gary Mamon (gam AAT iap.fr)"""
    
    # initialization file
    if ini_dir[0:len(home_dir)] != home_dir:
        ini_dir = home_dir + ini_dir
    if ini_dir[-1] != '/':
        ini_dir += '/'
    ini_file = ini_dir + prefix + '.ini'
    
    # initialize possibly missing variables
    
    rfid_dark = None
    tracermodel = []
    anismodel = []
    ilopflag = 0
    coord_type = None # for compatibility with old MAMPOSSt
    
    # read file line by line
    if verbose > 0:
        print("opening file ...")
    with open(ini_file) as f:
        # initialize dictionary of free parameter bounds
        dict_bounds = {}
        
        # loop over lines
        for i, line in enumerate(f):
            # omit comments and lines without assignments
            if line[0] == '#':
                continue
            if '=' not in line:
                continue
            
            # split line into words
            words = line.split(' ')
            words[-1] = words[-1].strip('\n')
            
            # read variables and values
            var = words[0]
            if len(words) == 3:
                val = words[2]
            elif len(words) > 3:
                val = words[2:]
            else:
                val = None
                
            if verbose >= 2:
                print(i,var,val)
            
            # save important flags and models (for corner figure)
            if var == 'components':
                components = val
            if var == 'darknormflag':
                darknormflag = float(val)
            elif var == 'darkscaleflag':
                darkscaleflag = int(val)
            elif var == 'darktotflag':
                darktotflag = int(val)
            elif var == 'anisflag':
                anisflag = int(val)
            elif var == 'ilopflag':
                ilopflag = int(val)
            elif var == 'darkmodel':
                darkmodel = val
            elif var == 'tracermodel': # always save in list
                tracermodel = [val]
            elif 'tracermodel' in var:
                tracermodel.append(val)
            elif var == 'anismodel': # always save in list
                anismodel = [val]
            elif 'anismodel' in var:
                anismodel.append(val)
            elif var == 'rfid_dark':
                rfid_dark = val
            elif var == 'coord_type':
                coord_type = val
            elif var == 'data_directory':
                data_dir = val
            elif var == 'data_file':
                data_file = val
             
            # build dictionary of free parameter bounds
            if var[0:6] == 'param[':
                # if words[4] != words[3]:
                param = var[6:-1]
                dict_bounds[param] = [float(words[3]),float(words[4])]
            if var == 'rand_seed':
                break
        if data_dir == '.':
            data_dir = ini_dir
        if data_dir[-1] != '/':
            data_dir += '/'
        data_file = data_dir + data_file 
        if verbose > 0:
            print("exiting ReadIni ...")
                    
        if labels:
            dict_labels = Labels(list(dict_bounds.keys()),components,
                                   darknormflag,darkscaleflag,darktotflag,
                                   anisflag,ilopflag,
                                   darkmodel,tracermodel,anismodel,rfid_dark,
                                   coord_type=coord_type,
                                   verbose=verbose)
            if verbose > 0:
                print("dict_labels=",dict_labels)
            return dict_bounds, dict_labels, data_file
        else:
            return dict_bounds, data_file    
            
def Labels(param_names, components, darknormflag, darkscaleflag, darktotflag, anisflag, 
           ilopflag, darkmodel, tracermodel, anismodel, rfid_dark, coord_type,
           verbose=0):
    """Prepare labels for corner plot
    
    arguments:
        param_names: list of parameter names
        components: list of components
        darknormflag
        darkscaleflag
        darktotflag
        anisflag
        ilopnormflag
        darkmodel
        tracermodel (list by component)
        anismodel (list by component)
        rfid_dark (fiducial radis for normalization
                   
    returns:
        dict_labels: dictionary of labels
        
    author: Gary Mamon (gam AAT iap.fr)"""
        
    if verbose > 0:
        print("in Labels...")
    # dark or total parameters (according to darktotflag)
    if darknormflag == -1.:
        if darktotflag == 1:
            dict_label_norm = '$\log\,r_\mathrm{vir}^\mathrm{dark}\ (\mathrm{kpc})$'
        else:
            dict_label_norm = '$\log\,r_\mathrm{vir}\ (\mathrm{kpc})$'
    elif darknormflag == 0.:
        if darktotflag == 1:
            dict_label_norm = '$\log\,M_\mathrm{vir}^\mathrm{dark}\ (\mathrm{kpc})$'
        else:
            dict_label_norm = '$\log\,M_\mathrm{vir}\ (\mathrm{kpc})$'
    elif rfid_dark is not None:
        if darktotflag == 1:
            dict_label_norm = '$\log\,M_\mathrm{dark}(' + str(rfid_dark) \
                                + ')\ [\mathrm{M}_\odot]$'
        else:
            dict_label_norm = '$\log\,M(' + str(rfid_dark) + ')\ [\mathrm{M}_\odot]$'
    else:
        raise ValueError('Cannot understand darknormflag=' + str(darknormflag)
                        + ' with rfid_dark=' + str(rfid_dark))
    if darkscaleflag == 1:
        if darkmodel in ['Sersic','Sérsic','PS']:
            dict_label_darkscale = '$\log\,R_\mathrm{e} \ (\mathrm{kpc})$'
        else:
            dict_label_darkscale = '$\log\,r_\mathrm{-2}\ (\mathrm{kpc})$'
    elif darkscaleflag == 2:
        if darktotflag == 1:
            dict_label_darkscale = '$\log\,c_{200}^\mathrm{dark}$'
        else:
            dict_label_darkscale = '$\log\,c_{200}$'
    else:
        raise ValueError('Cannot understand darkscaleflag=' + str(darkscaleflag))
    
    dict_label_darkpar3 = None
    if darkmodel in ['Sersic','Sérsic','Einasto']:
        dict_label_darkpar2 = '$n_\mathrm{' + darkmodel + '}$'
    elif darkmodel == 'gNFW':
        dict_label_darkpar2 = r'$\gamma$'
    elif darkmodel == 'ggNFW':
        dict_label_darkpar2 = r'$\gamma_\mathrm{inner}$'
        dict_label_darkpar3= r'$\gamma_\mathrm{outer}$'
    elif darkmodel == 'tNFW':
        dict_label_darkpar2 = '$\log\,r_\mathrm{trunc}\ (\mathrm{kpc})$'
    else:
        dict_label_darkpar2 = 'darkpar2'

    dict_label_lbhmass = '$\log\,M_\mathrm{BH}\ (\mathrm{M}_\odot)$'
    # tracer radii and tracer 2nd parameters are split by components,
    # even if only a single component.
    dict_label_ltracerradius = []
    dict_label_ltracermass = []
    dict_label_lanis0 = []
    dict_label_lanisinf = []
    dict_label_lanisradius = []
    dict_label_tracerpar2 = []
    # dict_label_meanltracerradius = []
    # dict_label_sigltracerradius = []
    if verbose > 0:
        print("components =",components)
    if coord_type in ['astro','gaia']:
        size_unit = "'"
    else:
        size_unit = "\mathrm{kpc}"
    for j, comp in enumerate(components):
        if verbose > 0:
            print("j comp = ", j, comp)
            print("tracermodel = '" + tracermodel[j] + "'")
        if comp != 'all':
            dict_label_ltracerradius.append("$\log\,r_\mathrm{" + comp
                                            + "}\ (" + size_unit + ")$")
            dict_label_ltracermass.append('$\log\,M_\mathrm{' + comp
                                            + '}\ (\mathrm{M}_\odot)$')
            dict_label_lanisradius.append(r"$\log\,r_\beta^\mathrm{" + comp 
                                          + "}\ (" + size_unit + ")$") 
            if tracermodel[j] in ['Sersic','Sérsic','einasto','Einasto']:
                dict_label_tracerpar2.append('$n_\mathrm{' + tracermodel[j]
                                             + ',' + comp + '}$')
            else:
                dict_label_tracerpar2.append('tracerpar2$_\mathrm{' 
                                             + comp + '}$')
            if anisflag == 0:
                dict_label_lanis0.append(r'$\log(\sigma_r/\sigma_\theta)_0'
                                    + '^\mathrm{' + comp + '}$')
                dict_label_lanisinf.append(r'$\log(\sigma_r/\sigma_\theta)_\infty,'
                                    + '^\mathrm{' + comp + '}$')
            elif anisflag == 1:
                dict_label_lanis0.append(r'$\beta_0^\mathrm{' + comp + '}$')
                dict_label_lanisinf.append(r'$\beta_\infty^\mathrm{,' + comp 
                                           + '}$')
            elif anisflag == 2:
                dict_label_lanis0.append(r'$\beta_\mathrm{sym,0}^\mathrm{' 
                                    + comp + '}$')
                dict_label_lanisinf.append(r'$\beta_\mathrm{sym,\infty}^\mathrm{' 
                                    + comp + '}$')
            elif anisflag == 22:
                dict_label_lanis0.append(r'$2\,\beta_\mathrm{sym,0}^\mathrm{'
                                         + comp + '}$')
                dict_label_lanisinf.append(r'$2\,\beta_\mathrm{sym,\infty}'
                                         + '^\mathrm{' + comp + '}$')
            else:
                raise ValueError('Cannot understand anisflag=' + str(anisflag))
        else:
            dict_label_ltracerradius.append("$\log\,r_mathrm{tr}\ (" + size_unit + ")$") 
            dict_label_ltracermass.append('$\log\,M_\mathrm{tr}\ (\mathrm{M})_\odot)$')
            dict_label_lanisradius.append(r"$\log\,r_\beta\ (" + size_unit + ")$") 
            if tracermodel in ['Sersic','Sérsic']:
                dict_label_tracerpar2 = '$n_\mathrm{' + tracermodel + '}$'
            else:
                dict_label_tracerpar2 = 'tracerpar2'    
            if anisflag == 0:
                dict_label_lanis0 = r'$\log(\sigma_r/\sigma_\theta)_0$'
                dict_label_lanisinf = r'$\log(\sigma_r/\sigma_\theta)_\infty$'
            elif anisflag == 1:
                dict_label_lanis0 = r'$\beta_0$'
                dict_label_lanisinf = r'$\beta_\infty$'
            elif anisflag == 2:
                dict_label_lanis0 = r'$\beta_\mathrm{sym,0}$'
                dict_label_lanisinf = r'$\beta_\mathrm{sym,\infty}$'
            elif anisflag == 22:
                dict_label_lanis0 = r'$2\,\beta_\mathrm{sym,0}$'
                dict_label_lanisinf = r'$2\,\beta_\mathrm{sym,\infty}$'
            else:
                raise ValueError('Cannot understand anisflag=' + str(anisflag))
        
    # additional parameters

    # initialize dictionary of labels
    dict_labels = {}
    
    # loop over parameter names: IN THIS ORDER!
    for i, param in enumerate(param_names):
        if param == 'norm':
            dict_labels[param] = dict_label_norm
        elif param == 'darkscale':
            dict_labels[param] = dict_label_darkscale
        elif param == 'darkpar2':
            dict_labels[param] = dict_label_darkpar2
        elif param == 'darkpar3':
            dict_labels[param] = dict_label_darkpar3
        elif param == 'ltracerradius':
            dict_labels[param] = dict_label_ltracerradius
        elif 'ltracerradius' in param:
            comp = param[param.index('_')+1:]
            icomp = components.index(comp)
            dict_labels[param] = dict_label_ltracerradius[icomp]
        elif param in ['ltracermass','ltracermasstot']:
            dict_labels[param] = dict_label_ltracermass
        elif 'ltracermass' in param:
            comp = param[param.index('_')+1:]
            icomp = components.index(comp)
            dict_labels[param] = dict_label_ltracermass[icomp]
        elif param == 'tracerpar2':
            dict_labels[param] = dict_label_tracerpar2
        elif 'tracerpar2' in param:
            comp = param[param.index('_')+1:]
            icomp = components.index(comp)
            dict_labels[param] = dict_label_tracerpar2[icomp]
        elif param == 'lanis0':
            dict_labels[param] = dict_label_lanis0
        elif 'lanis0' in param:
            comp = param[param.index('_')+1:]
            icomp = components.index(comp)
            dict_labels[param] = dict_label_lanis0[icomp]
        elif param == 'lanisinf':
            dict_labels[param] = dict_label_lanisinf
        elif 'lanisinf' in param:
            comp = param[param.index('_')+1:]
            icomp = components.index(comp)
            dict_labels[param] = dict_label_lanisinf[icomp]
        elif param == 'lanisradius':
            dict_labels[param] = dict_label_lanisradius
        elif 'lanisradius' in param:
            comp = param[param.index('_')+1:]
            icomp = components.index(comp)
            dict_labels[param] = dict_label_lanisradius[icomp]
        elif param == 'lbhmass':
            dict_labels[param] = dict_label_lbhmass
        elif param == 'PMRA0':
            dict_labels[param] = r'$\mathrm{PM}_\mathrm{RA}^\mathrm{bulk}\,\left({\mathrm{mas}\over\mathrm{yr}}\right)$'
        elif param == 'PMDec0':
            dict_labels[param] = r'$\mathrm{PM}_\mathrm{Dec}^\mathrm{bulk}\,\left({\mathrm{mas}\over\mathrm{yr}}\right)$'
        elif param == 'vLOS0':
            dict_labels[param] = r'$v_\mathrm{LOS}^\mathrm{bulk}\,\left({\mathrm{km}\over\mathrm{s}}\right)$'
        elif (ilopflag == 1) & (param == 'ilopnorm'):
            dict_labels[param] = r'$\log\left({N_\mathrm{field}\over N_\mathrm{sys}}\right)$'
        else:
            dict_labels[param] = param
    if verbose > 0:
        print("\nFinal dict_labels=\n")
        for k,v in dict_labels.items():
            print(k,v)

    # return dictionary of parameter_name:label
    return dict_labels

def PlotCorner(prefix,ini_dir,chain_dir,chain_root_dir='NOSAVE/COSMOMC/chains/',
               draw_labels=True,labels=None,
               label_size=None,title=None,title_size=None,gauss_prior=None,
               fullRange=True,verbose=0,truths2=None,burnin=0,
               plot=True):
    """
    1) Read .ini file and extract parameters
    2) Prepare labels
    3) Read data
    4) Filter data for free parameters
    5) Write file of Bayesian evidence (single line)
        prefix N_free -ln L_MLE AICc BIC
    6) Write file of best fit parameters
        
    7) Plot with corner on free parameterscd 
    
    arguments:
        prefix: file prefix (initialization and chains)
        ini_dir: directory of initialization (.ini) file (relative to home dir)
        chain_dir: directory of MCMC chains (relative to chain_root_dir)
        chain_root_dir: root directory of MCMC chains (relative to home dir)
        burnin: number of burnin chain elements to discard (0: for 2000*N_free)
        gauss_prior: boolean array of gaussian priors (instead of flat)
    author: Gary Mamon (gam AAT iap.fr)
    """
    
    import os
    
    # absolute path of ini file directory
    ini_dir = os.getenv("HOME") + '/' + ini_dir
    if ini_dir[-1] != '/':
        ini_dir += '/'
    
    if chain_dir[-1] != '/':
        chain_dir += '/'

    # parameter bounds & labels
    [dict_bounds,dict_labels,data_file] = ReadIni(prefix,ini_dir,labels=True,
                                        verbose=verbose)
    bounds = np.array(list(dict_bounds.values()))
    boundsFree = bounds[bounds[:,1]>bounds[:,0]]
    if verbose > 0:
        print("bounds=",bounds)
        print("boundsFree=",boundsFree)
        print("labels=",labels)
        print("dict_labels=",dict_labels)
        print("data_file=",data_file)
    provided_labels = False
    if labels is None:
        # create np.array from dict
        s = pd.Series(dict_labels)
        labels = s.values
    else:
        provided_labels = True
        
    # free parameters and labels thereof
    diffbounds = bounds[:,1]-bounds[:,0]
    i = np.arange(len(bounds)).astype(int)
    freeparam_nums = i[diffbounds>0]
    allparams = np.array(list(dict_labels.keys()))
    freeparams = allparams[freeparam_nums]

    if not provided_labels:
        if draw_labels:
            labels = labels[freeparam_nums]
        else:
            labels = np.arange(len(freeparam_nums)).astype(str)
    
    # chains
    if verbose > 0:
        print("chain_dir = ",chain_dir)
    chains_raw = ReadCosmoMCchains(prefix,mix=True,chain_root_dir=chain_root_dir,
                             chain_dir=chain_dir,
                             burnin=burnin,verbose=verbose)
    if verbose > 0:
        print("chains_raw shape = ",chains_raw.shape)
    
    # split between chains and –lnL
    nll = chains_raw[:,0]
    
    # filter chains for free parameters (+1 because column 0 is likelihood)
    chains_filter = chains_raw[:,freeparam_nums+1]
    
    # lowest -ln L
    x_MLE = chains_raw[np.argmin(chains_raw[:,0]),freeparam_nums+1]
    nll_min = nll.min()
    
    # number of data points

    N_data = int(subprocess.run(['grep','-cv','^#',data_file],
                                stdout=subprocess.PIPE).stdout)
    
    # bayesian evidence
    Nfree = len(freeparam_nums)
    Nchains = len(chains_filter)
    AICc = mu.AICc(-1*nll_min,Nfree,N_data)
    BIC  = mu.BIC (-1*nll_min,Nfree,N_data)
    if truths2 is not None:
        print("true values = ",truths2)
    # print("MLE values = ",x_MLE)
    
    # quantiles
    x_err = np.zeros(Nfree)
    x_median = np.zeros(Nfree)
    for i in range(Nfree):
        x = chains_filter[:,i]
        xq = np.quantile(x,[0.16,0.50,0.84])
        x_err[i] = (xq[2]-xq[0])/2
        x_median[i] = xq[1]
        # # FOLLOWING LINE HAS WRONG 1st COLUMN!
        # print(allparams[i],'%.3f'%x_MLE[i],'%.3f'%x_err[i])

    x_mean = np.mean(chains_filter,axis=0)
    
    # convert to strings with 3 digits after the decimal
    sx_MLE = np.array(['%.3f'%x for x in x_MLE])
    sx_mean = np.array(['%.3f'%x for x in x_mean])
    sx_median = np.array(['%.3f'%x for x in x_median])
    sx_err = np.array(['%.3f'%x for x in x_err])
    fit = np.transpose([freeparams,sx_MLE,sx_mean,sx_median,sx_err])
    # print("MLE mean err =",fit)

    # write statistics to file
    file_Bayes = prefix + '_Bayes.dat'
    np.savetxt(file_Bayes,
               np.column_stack(['%-30s'%prefix,'%3d'%Nfree,
                                '%8.2f'%nll_min,
                                '%8.2f'%AICc,'%8.2f'%BIC]),
               delimiter=' ',fmt='%s'
              )
    file_fit = prefix + '_fit.dat'
    np.savetxt(file_fit,fit,delimiter=' ',fmt='%s')
    
    # show output
    print("\nprefix Nfree -ln_MLE AICc BIC")
    subprocess.run(['cat',file_Bayes])
    
    print("\nprefix MLE mean median uncertainty")
    subprocess.run(['cat',file_fit])
    
    # corner plot
    # if draw_labels:
    if plot:
        if fullRange:
            plotRange=boundsFree
        else:
            plotRange=None
        corner(chains_filter,range=plotRange,labels=labels,label_size=label_size,
               truths=x_MLE,truths2=truths2,save_file=prefix,
               gauss_prior=gauss_prior,prior_display=2,
               title=title,title_size=title_size,verbose=verbose)
    if truths2 is not None:
        bias = x_MLE - truths2
        efficiency = x_err
        return np.array([bias, x_err]).T
    
def AnalyzeRuns(prefix,prefix2=None,imin=1,imax=None,ini_dir=None,chain_dir=None,
                params=None,true_values=None,methods=None,suffix=None,
                label_size=None,title_size=None,
                titlewoseed=False,
                plot=False,verbose=0):
    if home_dir not in ini_dir:
        ini_dir = home_dir + ini_dir
    Nparams = len(params)
    Nmeth = len(methods)
    if verbose >= 2:
        verbose2 = 1
    else:
        verbose2 = 0 
    # tab2str = np.zeros((3,3,4))
    biasall = np.zeros((imax,Nmeth,Nparams))
    uncertall = np.zeros((imax,Nmeth,Nparams))
    dict_labels={'logM':'$\log(M_\mathrm{vir}/\mathrm{M}_\odot)$',
                 'logc':'$\log\,c_\mathrm{dark}$',
                 'logrtr':'$\log(r_\mathrm{tr}/\mathrm{kpc})$',
                 'betasym0':r'$\beta_0^\mathrm{sym}$',
                 'betasyminf':r'$\beta_\infty^\mathrm{sym}$',
                 'rbeta':r'$\log(r_\beta/\mathrm{kpc})$'                 
                 }
    labels = [dict_labels[p] for p in params]
    # loop over methods
    for j, dist in enumerate(methods):
        if dist == 'noD':
            dist2 = 'no distances'
        else:
            dist2 = dist
        bias = np.zeros((imax,3))
        ineff = np.zeros((imax,3))
        
        # loop over seeds
        for i in range(imin,imax+1):
            print("seed",i)
            if prefix2 is not None:
                fileprefix = prefix + '_' + dist + '_' + prefix2 + '_' + str(i)
            else:
                fileprefix = prefix + '_' + dist + '_' + str(i)
            print("fileprefix=",fileprefix)
            if titlewoseed:
                title = dist2
            else:
                title = dist2 + ' (seed=' + str(i) + ')'
            tab = PlotCorner(fileprefix,ini_dir=ini_dir,chain_dir=chain_dir,
                             draw_labels=True,labels=labels,
                             label_size=label_size,title_size=title_size,
                             truths2=true_values,fullRange=True,
                             title=title,
                             plot=plot,verbose=verbose2)
            bias[i-1,:] = tab.T[0]
            ineff[i-1,:] = tab.T[1]
            biasall[i-1,j,:] = tab.T[0]
            uncertall[i-1,j,:] = tab.T[1]
            print("bias = ",tab.T[0])
        bias_mean = np.mean(bias,axis=0)
        bias_std = np.std(bias,axis=0)
        ineff_mean = np.mean(ineff,axis=0)
        ineff_std = np.std(ineff,axis=0)
        print(dist)
        print("bias: mean sig = ", np.transpose([bias_mean,bias_std]))
        print("inefficiency: mean sig = ", np.transpose([ineff_mean,ineff_std]))

    bias_mean2 = np.mean(biasall,axis=0)
    ebias_mean2 = np.std(biasall,axis=0)/np.sqrt(imax)
    uncert_mean2 = np.mean(uncertall,axis=0)
    euncert_mean2 = np.std(uncertall,axis=0)/np.sqrt(imax)
    rms_mean2 = np.sqrt(np.mean(biasall*biasall,axis=0))
    os.chdir(ini_dir)
    for i, pref in enumerate(params):
        tabstats = np.array([bias_mean2[:,i],ebias_mean2[:,i],rms_mean2[:,i],
                             uncert_mean2[:,i],euncert_mean2[:,i]]).T
        np.savetxt(prefix + '_stats_' + suffix + '_' + params[i] +  '.txt',tabstats,
                   fmt='%.3f±%.3f %.3f %.3f±%.3f')

def autotick(xmin,xmax,nummajorticks=4,minmajorticks=2,maxmajorticks=6):
    integers = np.arange(10)
    log_integers = np.log10(integers)
   
    
    # smallest simple number 
    adiff = abs(xmax - xmin)
    difflog = np.log10(adiff/nummajorticks)
    exp_val = difflog
    if difflog < 0:
        exp_val -= 1
    mantissa = difflog - exp_val

    if mantissa < .15:
       ticks = 1
       smticks = 5
    elif   mantissa < .5:
      ticks = 2
      smticks = 4
    elif  mantissa < .85:
      ticks = 5
      smticks = 5
    else:
      ticks = 10
      smticks = 5
    print("mantissa ticks smticks=",mantissa,ticks,smticks)
     
    # step = ticks*10.0**exp_val
    # smticks = step/dsmall + 0.5
    
def autoticks2(xmin,xmax,nummajorticks=4,nummajorticks_min=3,nummajorticks_max=5,
               iall=True,i25=True,verbose=0):
    # integers = np.arange(10)
    # log_integers = np.log10(integers)
   
    # simple step
    adiff = abs(xmax - xmin)
    step_guess = adiff/nummajorticks
    step_round = autoround(step_guess,step=True,iall=iall,i25=i25,
                           verbose=verbose)
    
    if isinstance(step_round,np.ndarray):
        found = False
        for i, step in enumerate(step_round):
            xticks = interval_by_step_minmax(xmin, xmax, step)
            N = len(xticks)
            if verbose > 0:
                print("i step N xticks = ",i, step,N,xticks)
            if (N < nummajorticks_min) or (N > nummajorticks_max):
                continue
            else:
                found = True
                break
        if not found:
            xticks = np.array([])
    return xticks

def autoround(x,step_round=None,step=False,iall=False,i25=True,verbose=0):
    if i25:
        ints = np.array([0.5,1,2,2.5,4,5])
    else:
        ints = np.array([0.5,1,2,4,5])
    logints = np.log10(ints)
    # for i, li in enumerate(logints):
        
    if verbose> 0:
        print("x step_round=",x,step_round)
    if step_round is not None:
        x_round = np.round(x,np.abs(np.round(np.log10(step_round)).astype(int)))
    else:
        x_round = np.round(x,np.abs(np.round(np.log10(x)).astype(int)))
    if (x_round == 0) & step:
        x_round = autoround(x,x/10,verbose=verbose)
    # nearest step 
    if step:
        x_round_power = np.rint(np.log10(x_round))
        if verbose > 0:
            print("x_round_power=",x_round_power)
        adlogx = np.abs(np.log10(x_round) - x_round_power - logints)
        if iall:
            index = adlogx.argsort()
        else:
            index= adlogx.argmin()
        x_round = 10**x_round_power * ints[index]

    #     if x_round 
    if verbose> 0:
        print("x_round=",x_round)
    return x_round

def mantissa(x):
    return x - int(x)

def interval_by_step_minmax(xmin,xmax,step):
    kmin = int(xmin/step)
    if kmin*step < xmin:
        kmin += 1
    kmax = int(xmax/step)
    k = np.arange(kmin,kmax+1,1).astype(int)
    return step*k
    
# exp_val = difflog
# if difflog < 0:
#     exp_val -= 1
# mantissa = difflog - exp_val

# if mantissa < .15:
#    ticks = 1
#    smticks = 5
# elif   mantissa < .5:
#   ticks = 2
#   smticks = 4
# elif  mantissa < .85:
#   ticks = 5
#   smticks = 5
# else:
#   ticks = 10
#   smticks = 5
# print("mantissa ticks smticks=",mantissa,ticks,smticks)
 
# # step = ticks*10.0**exp_val
# # smticks = step/dsmall + 0.5
