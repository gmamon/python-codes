def getvarname(var):
    """Variable name string"""
    # author: Eliott Mamon

    # copy globals dictionary
    vars_dict = globals().copy()

    # loop over variables in dictinary and return variable string related to variable name
    lastFoundUnderscoreName = None
    for key in vars_dict:
        if vars_dict[key] is var:
            if key[0] is not '_':
                return key
            lastFoundUnderscoreName = key
    return lastFoundUnderscoreName

